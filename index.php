<?php
  require_once(dirname(__FILE__, 2).'/api/controller/auth.php');
  $authContoller = new authContoller();

  checkFolderPermission();

  print file_get_contents("index.page");

  function checkFolderPermission(){
    $error = false;
    if (!is_writable(dirname(__FILE__, 2).'/api')) {
      $error = true;
      print "folder admin-panel/api required 755 permission <br />";
    }

    if (!is_writable(dirname(__FILE__, 3).'/navFood/userFoodImage')) {
      $error = true;
      print "folder navFood/userFoodImage required 755 permission <br />";
    }

    if ($error) {
      //header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
      die();
    }
  }
?>
