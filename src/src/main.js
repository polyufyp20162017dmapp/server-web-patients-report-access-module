// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Vue from 'vue'
var Vue = require('vue')
import App from './App'
import VeeValidate from 'vee-validate'
import vueResource from 'vue-resource'
import VueMask from 'v-mask'

var VueMaterial = require('vue-material')

/* eslint-disable no-new */
Vue.use(VueMaterial)
Vue.use(VeeValidate)
Vue.use(vueResource)
Vue.use(VueMask)

Vue.http.options.emulateJSON = true

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
